provider "cloudflare" {
  # SET CLOUDFLARE_EMAIL
  # SET CLOUDFLARE_API_TOKEN variable
}

variable "zone_id" {
  default = "a546d08b217128b7a06b59d4f046bc24"
}

resource "cloudflare_record" "root" {
  zone_id  = var.zone_id
  name    = "@"
  value   = "6shore.net.gitlab.io"
  type    = "CNAME"
  proxied = true
}

resource "cloudflare_record" "www" {
  zone_id  = var.zone_id
  name    = "www"
  value   = "piseckydeviant.cz"
  type    = "CNAME"
  proxied = true
}

resource "cloudflare_record" "gitlab-pages" {
  zone_id  = var.zone_id
  name    = "_gitlab-pages-verification-code"
  value   = "gitlab-pages-verification-code=9ef98e3c7a0e122c45690a65f8f5243b"
  type    = "TXT"
}

resource "cloudflare_record" "_gitlab-pages" {
  zone_id  = var.zone_id
  name    = "gitlab-pages-verification-code"
  value   = "gitlab-pages-verification-code=9ef98e3c7a0e122c45690a65f8f5243b"
  type    = "TXT"
}
